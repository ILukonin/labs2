// task_6.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#define MAX_STR 20000 // ����� � ������� �����


#include <stdio.h>
#include <string.h>

int main()
{
	unsigned char buf[MAX_STR];		// ������ ����� ���: � buf ����� �������� ������. �������� ���������� first ���� �� buf
	char * first = buf;		// ��������� ������ - �������� ������� second, ���� ������� �� ��������
	char * second = buf;	// ����� ���, ��� �� second �� ����� � ������� first �������� �� ������� ��� \0 � �����.
	int i = 0;

	printf("Input string\n");
	fgets(buf,MAX_STR,stdin);

	if( buf[strlen(buf)-1] == '\n') //������ ��� \n � �����
	{
		buf[strlen(buf)-1] = '\0';
	}

	for(i = 0; i < strlen(buf); i++)
	{
		if( (*first == ' ') && ( *(first + 1) == ' ' ))
		{
			second = first;

			while( *second == ' ' )
			{
				second++;
			}

			memmove(first,second - 1, strlen(second-1) + 1); // +1 - ��������  \0. -1 - ��� ����,  ���� ���� ���� ������ �������.
		}

		first++;
	}

	//������ � ��� �� ���� ������ ������ �� ������ �������. ����� ������� ������ � ���������, ���� ��� ����.

	if( buf[0] == ' ') // ������
	{
		memmove( buf, buf+1, strlen( buf + 1) + 1 );
	}

	if( buf[strlen(buf)-1] == ' ') //� ����� ��� � ����� ������?
	{
		buf[strlen(buf)-1] = '\0';
	}


	printf("%s\n",buf);

	for(i=0;i<strlen(buf)+2;i++)  //������� �����������, ���� ����������������� ��������� 
	{
		printf("%d - %c - %d\n",buf[i],buf[i],i);
	}

	return 0;
}

